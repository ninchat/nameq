module github.com/ninchat/nameq

go 1.17

require (
	github.com/aws/aws-sdk-go v1.42.6
	github.com/fsnotify/fsnotify v1.5.1
)

require (
	github.com/jmespath/go-jmespath v0.4.0 // indirect
	golang.org/x/sys v0.0.0-20211116061358-0a5406a5449c // indirect
)
